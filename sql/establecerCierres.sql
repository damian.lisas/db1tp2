DROP FUNCTION IF EXISTS establecerCierres;
CREATE OR REPLACE FUNCTION establecerCierres() RETURNS boolean as $$
DECLARE
    indice int;
    i int;
    fechainicio date;
    fechacierre date;
    fechavto date;
BEGIN
    indice := 0;
    for i in 0..9 loop
        for j in 1..12 loop
            fechainicio := ('2018-' || j::text || '-01')::date;
            fechacierre := fechainicio + interval '1 month';
            fechavto := fechacierre + interval '10 days';
            INSERT INTO cierre VALUES(2018, j, i, fechainicio, fechacierre, fechavto);
        end loop;
    end loop;
    return true;
END;
$$ LANGUAGE PLPGSQL;