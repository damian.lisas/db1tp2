DROP FUNCTION IF EXISTS generarResumen;
CREATE OR REPLACE FUNCTION generarResumen(idcliente INT, desde text, hasta text) RETURNS boolean AS $$
DECLARE
    elnombre text;
    elcliente cliente%rowtype;
    comprasDelMes record;
    unacompra record;
    tar record;
    ultimoIndiceCabecera int;
    ultimoIndiceLinea int;
    fechadesde timestamp;
    fechahasta timestamp;
    vence timestamp;
    total decimal(8,2);
BEGIN
    /*Calculo la fecha de vencimiento*/
    fechadesde := to_timestamp(desde, 'yyyy-mm-dd');
    fechahasta := to_timestamp(hasta, 'yyyy-mm-dd');
    vence := fechahasta + interval '10 days';
    raise notice '%',vence;

    /*Traigo al cliente*/
    select * into elcliente from cliente c where c.nrocliente = idcliente;

    /*Traigo el indice de la ultima cabecera*/
    SELECT MAX(nroresumen) INTO ultimoIndiceCabecera FROM cabecera;
    /*Valido el 0*/
    if ultimoIndiceCabecera isnull then
        ultimoIndiceCabecera := 0;
    end if;

    /*Seteo el ultimo indice de los detalles*/
    select max(nrolinea) into ultimoIndiceLinea from detalle;
    if ultimoIndiceLinea isnull then
        ultimoIndiceLinea := 0;
    end if;

    /*Itero cada tarjeta del cliente*/
    for tar in SELECT * FROM tarjeta WHERE nrocliente = elcliente.nrocliente LOOP
        ultimoIndiceCabecera := ultimoIndiceCabecera + 1;
        
        /*Calculo el total de las compras*/
        select sum(monto) into total FROM compra
            WHERE compra.fecha >= fechadesde 
            AND compra.fecha  < fechahasta 
            AND compra.pagado = false 
            AND compra.nrotarjeta = tar.nrotarjeta;

        /*Inserto la cabecera del resumen*/
        INSERT INTO cabecera values( ultimoIndiceCabecera,
                                    elcliente.nombre,
                                    elcliente.apellido,
                                    elcliente.domicilio,
                                    tar.nrotarjeta,
                                    fechadesde,
                                    fechahasta,
                                    vence,
                                    total);

        /*Inserto los detalles del resumen*/
        FOR unacompra IN SELECT * FROM compra WHERE compra.fecha >= fechadesde AND compra.fecha < fechahasta AND compra.pagado = false AND compra.nrotarjeta = tar.nrotarjeta LOOP
            ultimoIndiceLinea := ultimoIndiceLinea + 1;

            select nombre into elnombre from comercio where nrocomercio = unacompra.nrocomercio;
            INSERT INTO detalle values( ultimoIndiceCabecera,
                                        ultimoIndiceLinea,
                                        unacompra.fecha,
                                        elnombre,
                                        unacompra.monto);
        END LOOP;

    end loop;
    return true;
END;


$$ LANGUAGE PLPGSQL;
