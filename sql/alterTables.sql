ALTER TABLE cliente     add CONSTRAINT cliente_pk   PRIMARY KEY (nrocliente);
ALTER TABLE tarjeta     add CONSTRAINT tarjeta_pk   PRIMARY KEY (nrotarjeta);
ALTER TABLE comercio    add CONSTRAINT comercio_pk  PRIMARY KEY (nrocomercio);
ALTER TABLE compra      add CONSTRAINT compra_pk    PRIMARY KEY (nrooperacion);
ALTER TABLE rechazo     add CONSTRAINT rechazo_pk   PRIMARY KEY (nrorechazo);
ALTER TABLE cierre      add CONSTRAINT cierre_pk    PRIMARY KEY (año,mes,terminacion);
ALTER TABLE cabecera    add CONSTRAINT cabecera_pk  PRIMARY KEY (nroresumen);
ALTER TABLE detalle     add CONSTRAINT detalle_pk   PRIMARY KEY (nroresumen,nrolinea);
ALTER TABLE alerta      add CONSTRAINT alerta_pk    PRIMARY KEY (nroalerta);

ALTER TABLE tarjeta     add CONSTRAINT tarjeta_nrocliente_fk    FOREIGN KEY (nrocliente)    REFERENCES cliente(nrocliente);
ALTER TABLE compra      add CONSTRAINT compra_nrotarjeta_fk     FOREIGN KEY (nrotarjeta)    REFERENCES tarjeta(nrotarjeta);
ALTER TABLE compra      add CONSTRAINT compra_nrocomercio_fk    FOREIGN KEY (nrocomercio)   REFERENCES comercio(nrocomercio);
ALTER TABLE cabecera    add CONSTRAINT cabecera_nrotarjeta_fk   FOREIGN KEY (nrotarjeta)    REFERENCES tarjeta(nrotarjeta);
ALTER TABLE detalle     add CONSTRAINT detalle_nroresumen_fk    FOREIGN KEY (nroresumen)    REFERENCES cabecera(nroresumen);
ALTER TABLE alerta      add CONSTRAINT alerta_nrotarjeta_fk     FOREIGN KEY (nrotarjeta)    REFERENCES tarjeta(nrotarjeta);
ALTER TABLE alerta      add CONSTRAINT alerta_nrorechazo_fk     FOREIGN KEY (nrorechazo)    REFERENCES rechazo(nrorechazo);