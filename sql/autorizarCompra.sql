CREATE OR REPLACE FUNCTION autorizacionCompra(nrotar CHAR(16), codseg CHAR(4), nrocom INT, cmonto DECIMAL(8,2)) RETURNS BOOLEAN AS $$
    DECLARE
        resultado record;
        ultimacompra int;
        ultimorechazo int;
    BEGIN
        select max(nrorechazo) into ultimorechazo from rechazo;

        if ultimorechazo ISNULL THEN
            raise notice 'ultimo rechazo null';
            ultimorechazo := 0;
        end if;

        SELECT * INTO resultado FROM tarjeta WHERE nrotarjeta = nrotar;

        IF NOT FOUND THEN
                INSERT INTO rechazo VALUES (ultimorechazo + 1, nrotar, nrocom, CURRENT_TIMESTAMP, cmonto, '?tarjeta no válida o no vigente');
                RETURN FALSE;
        ELSIF (resultado.codseguridad != codseg) THEN
                INSERT INTO rechazo VALUES (ultimorechazo + 1, nrotar, nrocom, CURRENT_TIMESTAMP, cmonto, '?código de seguridad inválido');
                RETURN FALSE;
        ELSIF (TO_CHAR(CURRENT_TIMESTAMP,'YYYYMMDD') > resultado.validahasta ) THEN
                INSERT INTO rechazo VALUES (ultimorechazo + 1, nrotar, nrocom, CURRENT_TIMESTAMP, cmonto, '?plazo de vigencia expirado');
                RETURN FALSE;
        ELSIF (resultado.estado = 'suspendida') THEN   
                INSERT INTO rechazo VALUES (ultimorechazo + 1,nrotar, nrocom, CURRENT_TIMESTAMP, cmonto, '?la tarjeta se encuentra suspendida');
                RETURN FALSE;
        ELSIF ( (SELECT SUM(monto) FROM compra WHERE nrotarjeta = nrotar and pagado = false) + cmonto > resultado.limitecompra ) THEN
                raise notice 'Se supero el limite de la tarjeta';
                INSERT INTO rechazo VALUES (ultimorechazo + 1, nrotar, nrocom, CURRENT_TIMESTAMP, cmonto, '?supera el límite de tarjeta');
                RETURN FALSE;
        ELSE
                /*TODO: Agregar validacion en caso de que no existan compras todavia*/
                select MAX(nrooperacion) into ultimacompra from compra;
                if ultimacompra ISNULL THEN
                    ultimacompra := 0;
                end if;
                INSERT INTO compra VALUES(ultimacompra + 1, nrotar, nrocom, CURRENT_TIMESTAMP, cmonto, FALSE);
        END IF;
    RETURN TRUE;
    END;
$$ LANGUAGE plpgsql;
