package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
	"time"

	_ "github.com/lib/pq"
)

func main() {
	var input int
	salir := 9

	fmt.Printf("Creando base de datos...\n")
	createDatabase()
	fmt.Printf("Base de datos creada!\n")

	fmt.Printf("Conectando a la base de datos..\n")
	db, err := connectDatabase()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	fmt.Printf("Base de datos conectada!\n")

	/* Mientras el usuario no elija la opcion de salir... */
	for input != salir {
		imprimirInterface()
		fmt.Scanf("%d", &input)
		switch input {
		case 1:
			crearTablas(db)
		case 2:
			alterTablas(db)
		case 3:
			insertarDatos(db)
		case 4:
			crearFuncionesYTriggers()
		case 5:
			realizarCompras(db)
		case 6:
			establecerCierres(db)
		case 7:
			generarResumen(db)
		case 8:
			realizarCompra(db)
		case salir:
			fmt.Printf("Adios\n")
		default:
			fmt.Printf("Opcion no reconocida\n")
		}
		CallClear()
	}
}

func imprimirInterface() {
	fmt.Printf("*********************************\n")
	fmt.Printf("*           CLI SQL             *\n")
	fmt.Printf("*********************************\n")
	fmt.Printf("* 1. Crear tablas               *\n")
	fmt.Printf("* 2. Alter tablas               *\n")
	fmt.Printf("* 3. Insertar datos             *\n")
	fmt.Printf("* 4. Crear triggers y funciones *\n")
	fmt.Printf("* 5. Realizar compras           *\n")
	fmt.Printf("* 6. Establecer cierres         *\n")
	fmt.Printf("* 7. Generar resumen            *\n")
	fmt.Printf("* 8. Realizar compra            *\n")
	fmt.Printf("* 9. Salir                      *\n")
	fmt.Printf("*********************************\n")
	fmt.Printf("	Elija una opcion: ")
}

func createDatabase() {
	db, err := sql.Open("postgres", "user=dergon password=dergon host=localhost dbname=dergon sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	fmt.Printf("Eliminando 'prueba'\n")
	_, _ = db.Exec(`DROP DATABASE prueba`)
	fmt.Printf("Creando nueva base de datos 'prueba'\n")
	_, err = db.Exec(`CREATE DATABASE prueba`)
	if err != nil {
		log.Fatal(err)
	}
}

func connectDatabase() (*sql.DB, error) {
	return sql.Open("postgres", "user=dergon password=dergon host=localhost dbname=prueba sslmode=disable")
}

func crearTablas(db *sql.DB) {
	querys := []string{"CREATE TABLE cliente(nrocliente INT,nombre TEXT,apellido TEXT,domicilio TEXT,telefono CHAR(12))",
		"CREATE TABLE tarjeta(nrotarjeta CHAR(16),nrocliente INT,validadesde CHAR(6),validahasta CHAR(6),codseguridad CHAR(4),limitecompra DECIMAL(8,2),estado CHAR(10))",
		"CREATE TABLE comercio(nrocomercio INT,nombre TEXT,domicilio TEXT,	codigopostal CHAR(8),telefono CHAR(12))",
		"CREATE TABLE compra(nrooperacion INT,nrotarjeta CHAR(16),nrocomercio INT,fecha TIMESTAMP,monto DECIMAL(7,2),pagado boolean)",
		"CREATE TABLE rechazo(nrorechazo INT,nrotarjeta CHAR(16),nrocomercio INT,fecha TIMESTAMP,monto DECIMAL(7,2),motivo TEXT)",
		"CREATE TABLE cierre(año INT,mes INT,terminacion INT,fechainicio date,fechacierre date,fechavto date)",
		"CREATE TABLE cabecera (nroresumen INT,nombre TEXT,apellido TEXT,domicilio TEXT,nrotarjeta CHAR(16),desde date,hasta date,vence date,total DECIMAL(8,2))",
		"CREATE TABLE detalle (nroresumen INT,nrolinea INT,fecha DATE,nombrecomercio TEXT,monto DECIMAL(7,2))",
		"CREATE TABLE alerta (nroalerta INT,nrotarjeta CHAR(16),fecha TIMESTAMP,nrorechazo INT,codalerta INT,descripcion TEXT)",
		"CREATE TABLE consumo(	nrotarjeta CHAR(16),codseguridad CHAR(4),nrocomercio INT,monto DECIMAL(7,2))"}
	execSlice(querys, db)
}
func alterTablas(db *sql.DB) {
	querys := []string{"ALTER TABLE cliente add CONSTRAINT cliente_pk PRIMARY KEY (nrocliente)",
		"ALTER TABLE tarjeta  add CONSTRAINT tarjeta_pk  PRIMARY KEY (nrotarjeta)",
		"ALTER TABLE comercio add CONSTRAINT comercio_pk PRIMARY KEY (nrocomercio)",
		"ALTER TABLE compra   add CONSTRAINT compra_pk   PRIMARY KEY (nrooperacion)",
		"ALTER TABLE rechazo  add CONSTRAINT rechazo_pk  PRIMARY KEY (nrorechazo)",
		"ALTER TABLE cierre   add CONSTRAINT cierre_pk   PRIMARY KEY (año,mes,terminacion)",
		"ALTER TABLE cabecera add CONSTRAINT cabecera_pk PRIMARY KEY (nroresumen)",
		"ALTER TABLE detalle  add CONSTRAINT detalle_pk  PRIMARY KEY (nroresumen,nrolinea)",
		"ALTER TABLE alerta   add CONSTRAINT alerta_pk   PRIMARY KEY (nroalerta)",
		"ALTER TABLE tarjeta  add CONSTRAINT tarjeta_nrocliente_fk  FOREIGN KEY (nrocliente)  REFERENCES cliente(nrocliente)",
		"ALTER TABLE compra   add CONSTRAINT compra_nrotarjeta_fk   FOREIGN KEY (nrotarjeta)  REFERENCES tarjeta(nrotarjeta)",
		"ALTER TABLE compra   add CONSTRAINT compra_nrocomercio_fk  FOREIGN KEY (nrocomercio) REFERENCES comercio(nrocomercio)",
		"ALTER TABLE cabecera add CONSTRAINT cabecera_nrotarjeta_fk FOREIGN KEY (nrotarjeta)  REFERENCES tarjeta(nrotarjeta)",
		"ALTER TABLE detalle  add CONSTRAINT detalle_nroresumen_fk  FOREIGN KEY (nroresumen)  REFERENCES cabecera(nroresumen)",
		"ALTER TABLE alerta   add CONSTRAINT alerta_nrotarjeta_fk   FOREIGN KEY (nrotarjeta)  REFERENCES tarjeta(nrotarjeta)",
		"ALTER TABLE alerta   add CONSTRAINT alerta_nrorechazo_fk   FOREIGN KEY (nrorechazo)  REFERENCES rechazo(nrorechazo)"}

	execSlice(querys, db)
}

func insertarDatos(db *sql.DB) {
	querys := []string{"INSERT INTO comercio VALUES(1, 'Carrefour', 'Pte. Peron 150', 'C1001KTX', '541146671414')",
		"INSERT INTO comercio VALUES(2,  'Dia',           'Primera Junta 1265',       'C1417KAI', '541146642080')",
		"INSERT INTO comercio VALUES(3,  'Nike',          'Guayanas 1996',            'C1158AAI', '541144519875')",
		"INSERT INTO comercio VALUES(4,  'Adidas',        'Colombia 1337',            'C1425BGO', '541144511053')",
		"INSERT INTO comercio VALUES(5,  'Puma',          'Nicaragua 261',            'C106PAAI', '540232359863')",
		"INSERT INTO comercio VALUES(6,  'Axa',           'Tribulato 500',            'C1663KTX', '541146663259')",
		"INSERT INTO comercio VALUES(7,  'Levis',         'Balvastro 1532',           'C1663KTX', '541116932689')",
		"INSERT INTO comercio VALUES(8,  'Coto',          'Av. Senador Moron 725',    'C1417DKF', '541165238941')",
		"INSERT INTO comercio VALUES(9,  'Falabella',     'Av. Constituyentes 2016',  'C1002DJP', '541136295847')",
		"INSERT INTO comercio VALUES(10, 'Sodimac',       'Av. Santa Fe 102',         'C1069AAI', '541191758266')",
		"INSERT INTO comercio VALUES(11, 'Kiosco DobleA', 'Pte. Peron 2032',          'C1213EKF', '541126548796')",
		"INSERT INTO comercio VALUES(12, 'Carniceria',    'San Juan 614',             'C1158DJP', '541136251499')",
		"INSERT INTO comercio VALUES(13, 'Verduleria',    'Santiago del Estero 1948', 'C1425EKF', '541103154890')",
		"INSERT INTO comercio VALUES(14, 'Raphsodia',     'Entre Rios 194',           'C1005BJN', '541148069572')",
		"INSERT INTO comercio VALUES(15, '47 Street',     'Paso de los Patos 948',    'C1001EVE', '541136251480')",
		"INSERT INTO comercio VALUES(16, 'Tascani',       'Cochabamba 826',           'C1663KAI', '541198746232')",
		"INSERT INTO comercio VALUES(17, 'Herencia',      'Av. Libertador 3100',      'C1213EKF', '541126984526')",
		"INSERT INTO comercio VALUES(18, 'Kosiuko',       'Cordoba 447',              'C1665BJN', '541187026031')",
		"INSERT INTO comercio VALUES(19, 'Sony',          'La Rioja 523',             'C1009AAD', '541102031598')",
		"INSERT INTO comercio VALUES(20, 'Easy',          'Las Heras 945',            'C1665EVE', '541167853200')",
		"INSERT INTO cliente VALUES(1,  'Sebastian',   'Posadas',     'Beltran 971',        '541540795143')",
		"INSERT INTO cliente VALUES(2,  'Cristian',    'Pochidis',    'Kennedy 202',        '541540795144')",
		"INSERT INTO cliente VALUES(3,  'Andres',      'Posdata',     'Groussac 99',        '541540795145')",
		"INSERT INTO cliente VALUES(4,  'Damian',      'Lisas',       'Gelly Obes 221',     '541540795146')",
		"INSERT INTO cliente VALUES(5,  'Matias',      'Arriola',     'La Pampa 505',       '541540795147')",
		"INSERT INTO cliente VALUES(6,  'Juan',        'Bianchini',   'Brasil 205',         '541540795148')",
		"INSERT INTO cliente VALUES(7,  'Carlos',      'Urtubey',     'Uruguay 552',        '541540795149')",
		"INSERT INTO cliente VALUES(8,  'Yair',        'Ruiz Barbas', 'Ecuador 2018',       '541540795150')",
		"INSERT INTO cliente VALUES(9,  'Ricardo',     'Dominguez',   'Suiza 2235',         '541540795151')",
		"INSERT INTO cliente VALUES(10, 'Jorge',       'Gimenez',     'España 2024',        '541540795152')",
		"INSERT INTO cliente VALUES(11, 'Liz',         'None',        'Inglaterra 2005',    '541540795153')",
		"INSERT INTO cliente VALUES(12, 'Tiago',       'Gomez',       'Portugal 505',       '541540795154')",
		"INSERT INTO cliente VALUES(13, 'Benjamin',    'Lopetegui',   'Arabia 2045',        '541540795155')",
		"INSERT INTO cliente VALUES(14, 'Ariel',       'Zidane',      'URSS 1204',          '541540795156')",
		"INSERT INTO cliente VALUES(15, 'Carolina',    'Barria',      'Nueva Zelanda 2555', '541540795157')",
		"INSERT INTO cliente VALUES(16, 'Anabel',      'Barros',      'Peru 2123',          '541540795158')",
		"INSERT INTO cliente VALUES(17, 'Elizabeth',   'Birriten',    'Holanda 204',        '541540795159')",
		"INSERT INTO cliente VALUES(18, 'Miriam',      'Noct',        'Gran Bretaña 2002',  '541540795160')",
		"INSERT INTO cliente VALUES(19, 'Maximiliano', 'Berrio',      'Mexico 2512',        '541540795161')",
		"INSERT INTO cliente VALUES(20, 'Maximo',      'Niggo',       'Chile 2021',         '541540795162')",
		"INSERT INTO tarjeta VALUES('4347543729569502',  1,  '201801', '202101', '2594',  15800.00,   'vigente')",
		"INSERT INTO tarjeta VALUES('4916863946313661',  1,  '200905', '201205', '8547',  13700.00,   'anulada')",
		"INSERT INTO tarjeta VALUES('4916826986974213',  2,  '201706', '202006', '3683',  23500.00,   'vigente')",
		"INSERT INTO tarjeta VALUES('4916284621724645',  2,  '201606', '201906', '7992',  470080.00, 'suspendida')",
		"INSERT INTO tarjeta VALUES('4716855063738854',  3,  '201605', '201905', '7087',  260100.00,  'vigente')",
		"INSERT INTO tarjeta VALUES('4556575439340010',  4,  '201611', '201911', '2017',  96800.00,   'vigente')",
		"INSERT INTO tarjeta VALUES('4716545552610751',  5,  '201603', '201603', '2217',  21600.00,   'vigente')",
		"INSERT INTO tarjeta VALUES('4916851086716827',  6,  '201709', '202009', '9458',  612070.00, 'vigente')",
		"INSERT INTO tarjeta VALUES('4527336387895661',  7,  '201701', '202001', '7688',  488400.00,  'vigente')",
		"INSERT INTO tarjeta VALUES('4916851421476038',  8,  '201705', '202005', '1930',  829400.00,  'vigente')",
		"INSERT INTO tarjeta VALUES('4929317103413652',  9,  '201709', '202009', '8202',  304120.00,  'vigente')",
		"INSERT INTO tarjeta VALUES('4539903001039483',  10, '201710', '202010', '8802',  23570.00,   'vigente')",
		"INSERT INTO tarjeta VALUES('4024007163871535',  11, '201711', '202011', '8155',  27100.00,   'vigente')",
		"INSERT INTO tarjeta VALUES('4532946300414664',  12, '201712', '202012', '1045',  57240.00,   'vigente')",
		"INSERT INTO tarjeta VALUES('4929850684060336',  13, '201803', '202103', '4975',  87610.00,   'vigente')",
		"INSERT INTO tarjeta VALUES('4716141179530000',  14, '201801', '202101', '8544',  70515.00,   'vigente')",
		"INSERT INTO tarjeta VALUES('4556687147375749',  15, '201807', '202107', '9861',  93280.00,   'vigente')",
		"INSERT INTO tarjeta VALUES('4532338869283559',  16, '201804', '202104', '6896',  90650.00,   'vigente')",
		"INSERT INTO tarjeta VALUES('4539779993105700',  17, '201512', '201812', '3254',  44900.00,   'vigente')",
		"INSERT INTO tarjeta VALUES('4716543391247935',  18, '201612', '201912', '1083',  57800.00,   'vigente')",
		"INSERT INTO tarjeta VALUES('4716219247447769',  19, '201809', '202109', '3130',  79500.00,   'vigente')",
		"INSERT INTO tarjeta VALUES('4929631905109379',  20, '201802', '202102', '1169',  32300.00,   'vigente')",
		"INSERT INTO consumo VALUES ('4347543729569502','2594',1,  1200.02)",
		"INSERT INTO consumo VALUES ('4347543729569502','2594',5, 25200.02)",
		"INSERT INTO consumo VALUES ('4347543729569502','2594',2,   900.02)",
		"INSERT INTO consumo VALUES ('4347543729569502','2594',3,  8500.02)",
		"INSERT INTO consumo VALUES ('4916863946313661','8547',4,  2200.02)",
		"INSERT INTO consumo VALUES ('4916863946313661','8547',5,  2200.02)",
		"INSERT INTO consumo VALUES ('4916863946313661','8547',2,   400.02)",
		"INSERT INTO consumo VALUES ('4916863946313661','8547',1,   500.02)",
		"INSERT INTO consumo VALUES ('4916826986974213','3683',1,   150.02)",
		"INSERT INTO consumo VALUES ('4916826986974213','3683',2,   290.02)",
		"INSERT INTO consumo VALUES ('4916826986974213','3683',20,  450.02)",
		"INSERT INTO consumo VALUES ('4916826986974213','3683',11, 1890.02)",
		"INSERT INTO consumo VALUES ('4916284621724645','7992',3,   600.02)",
		"INSERT INTO consumo VALUES ('4916284621724645','7992',5,  1200.02)",
		"INSERT INTO consumo VALUES ('4916284621724645','7992',8,    90.02)",
		"INSERT INTO consumo VALUES ('4916284621724645','7992',11,  200.02)",
		"INSERT INTO consumo VALUES ('4716855063738854','7087',12, 2000.02)",
		"INSERT INTO consumo VALUES ('4716855063738854','7087',13, 2500.02)",
		"INSERT INTO consumo VALUES ('4716855063738854','7087',20, 3500.02)",
		"INSERT INTO consumo VALUES ('4716855063738854','7087',8,  7500.02)",
		"INSERT INTO consumo VALUES ('4556575439340010','2017',7,  3500.02)",
		"INSERT INTO consumo VALUES ('4556575439340010','2017',12, 2200.02)",
		"INSERT INTO consumo VALUES ('4556575439340010','2017',19, 4000.02)",
		"INSERT INTO consumo VALUES ('4556575439340010','2017',3,  9900.02)",
		"INSERT INTO consumo VALUES ('4716545552610751','2217',1,   750.02)",
		"INSERT INTO consumo VALUES ('4716545552610751','2217',1,  9500.02)",
		"INSERT INTO consumo VALUES ('4716545552610751','2217',7,   920.02)",
		"INSERT INTO consumo VALUES ('4716545552610751','2217',3,  1400.02)",
		"INSERT INTO consumo VALUES ('4916851086716827','9458',15,  600.02)",
		"INSERT INTO consumo VALUES ('4916851086716827','9458',16, 7200.02)",
		"INSERT INTO consumo VALUES ('4916851086716827','9458',18, 4500.02)",
		"INSERT INTO consumo VALUES ('4916851086716827','9458',20, 1600.02)",
		"INSERT INTO consumo VALUES ('4527336387895661','7688',1,   750.02)",
		"INSERT INTO consumo VALUES ('4527336387895661','7688',7, 99967.02)",
		"INSERT INTO consumo VALUES ('4527336387895661','7688',9,  5000.02)",
		"INSERT INTO consumo VALUES ('4527336387895661','7688',11,   80.02)",
		"INSERT INTO consumo VALUES ('4916851421476038','1930',5,   440.02)",
		"INSERT INTO consumo VALUES ('4916851421476038','1930',8,   250.02)",
		"INSERT INTO consumo VALUES ('4916851421476038','1930',11,  980.02)",
		"INSERT INTO consumo VALUES ('4916851421476038','1930',15,  830.02)",
		"INSERT INTO consumo VALUES ('4929317103413652','8202',12,  270.02)",
		"INSERT INTO consumo VALUES ('4929317103413652','8202',13,   20.02)",
		"INSERT INTO consumo VALUES ('4929317103413652','8202',20,  800.02)",
		"INSERT INTO consumo VALUES ('4929317103413652','8202',3,   500.02)",
		"INSERT INTO consumo VALUES ('4539903001039483','8802',18, 1800.02)",
		"INSERT INTO consumo VALUES ('4539903001039483','8802',17, 2900.02)",
		"INSERT INTO consumo VALUES ('4539903001039483','8802',20, 3000.02)",
		"INSERT INTO consumo VALUES ('4539903001039483','8802',5,  1500.02)",
		"INSERT INTO consumo VALUES ('4024007163871535','8155',8,   555.02)",
		"INSERT INTO consumo VALUES ('4024007163871535','8155',9,  7450.02)",
		"INSERT INTO consumo VALUES ('4024007163871535','8155',12,15300.02)",
		"INSERT INTO consumo VALUES ('4024007163871535','8155',13,18950.02)",
		"INSERT INTO consumo VALUES ('4532946300414664','1045',11,15200.02)",
		"INSERT INTO consumo VALUES ('4532946300414664','1045',17, 2200.02)",
		"INSERT INTO consumo VALUES ('4532946300414664','1045',20, 4400.02)",
		"INSERT INTO consumo VALUES ('4532946300414664','1045',1,   900.02)",
		"INSERT INTO consumo VALUES ('4929850684060336','4975',15,10000.02)",
		"INSERT INTO consumo VALUES ('4929850684060336','4975',13,24400.02)",
		"INSERT INTO consumo VALUES ('4929850684060336','4975',20, 1984.02)",
		"INSERT INTO consumo VALUES ('4929850684060336','4975',9, 15646.02)",
		"INSERT INTO consumo VALUES ('4716141179530000','8544',4,  1208.02)",
		"INSERT INTO consumo VALUES ('4716141179530000','8544',9,  2520.02)",
		"INSERT INTO consumo VALUES ('4716141179530000','8544',11, 9080.02)",
		"INSERT INTO consumo VALUES ('4716141179530000','8544',17, 1480.02)",
		"INSERT INTO consumo VALUES ('4556687147375749','9861',3,  1980.02)",
		"INSERT INTO consumo VALUES ('4556687147375749','9861',15, 2600.02)",
		"INSERT INTO consumo VALUES ('4556687147375749','9861',19,88520.02)",
		"INSERT INTO consumo VALUES ('4556687147375749','9861',20,98440.02)",
		"INSERT INTO consumo VALUES ('4532338869283559','6896',6,  1880.02)",
		"INSERT INTO consumo VALUES ('4532338869283559','6896',18, 8800.02)",
		"INSERT INTO consumo VALUES ('4532338869283559','6896',14, 4560.02)",
		"INSERT INTO consumo VALUES ('4532338869283559','6896',12, 1230.02)",
		"INSERT INTO consumo VALUES ('4539779993105700','3254',5,  1990.02)",
		"INSERT INTO consumo VALUES ('4539779993105700','3254',17, 1800.02)",
		"INSERT INTO consumo VALUES ('4539779993105700','3254',12, 1455.02)",
		"INSERT INTO consumo VALUES ('4539779993105700','3254',11,99912.02)",
		"INSERT INTO consumo VALUES ('4716543391247935','1083',7, 15488.02)",
		"INSERT INTO consumo VALUES ('4716543391247935','1083',12,12010.02)",
		"INSERT INTO consumo VALUES ('4716543391247935','1083',18,17840.02)",
		"INSERT INTO consumo VALUES ('4716543391247935','1083',19, 2500.02)",
		"INSERT INTO consumo VALUES ('4716219247447769','3130',14, 1500.02)",
		"INSERT INTO consumo VALUES ('4716219247447769','3130',15, 2100.02)",
		"INSERT INTO consumo VALUES ('4716219247447769','3130',18,  450.02)",
		"INSERT INTO consumo VALUES ('4716219247447769','3130',20,  800.02)",
		"INSERT INTO consumo VALUES ('4929631905109379','1169',1,  8500.02)",
		"INSERT INTO consumo VALUES ('4929631905109379','1169',8,  2700.02)",
		"INSERT INTO consumo VALUES ('4929631905109379','1169',10, 1800.02)",
		"INSERT INTO consumo VALUES ('4929631905109379','1169',11, 1100.02)"}

	execSlice(querys, db)
}

func crearFuncionesYTriggers() {
	out, err := exec.Command("./../scripts/crearTriggersYProcedimientos.sh", "dergon prueba").Output()
	if err != nil {
		fmt.Printf("%s", err)
	}
	fmt.Printf("%s", out)

}

func realizarCompras(db *sql.DB) {
	_, err := db.Query(`SELECT hacerCompras()`)
	if err != nil {
		fmt.Printf("Error. %v", err)
	} else {
		fmt.Printf("Compras realizadas segun los consumos!\n")
	}
}

func generarResumen(db *sql.DB) {
	var cliente int64
	var inicio, fin string
	fmt.Printf("Nro. Cliente: ")
	fmt.Scanf("%v", &cliente)
	fmt.Printf("\nInicio del periodo: ")
	fmt.Scanf("%v", &inicio)
	fmt.Printf("\nFin del periodo: ")
	fmt.Scanf("%v", &fin)
	fmt.Printf("\n")

	_, err := db.Query(`SELECT generarResumen($1,$2,$3)`, cliente, inicio, fin)
	if err != nil {
		fmt.Printf("Error. %v\n", err)
	} else {
		fmt.Printf("Resumen generado\n")
	}
}

func realizarCompra(db *sql.DB) {
	var tarjeta, codseguridad string
	var comercio, nrooperacion int64
	var resultado bool
	monto := 0.0

	fmt.Printf("\nNro. tarjeta: ")
	fmt.Scanf("%v", &tarjeta)
	fmt.Printf("\nCodigo de seguridad: ")
	fmt.Scanf("%v", &codseguridad)
	fmt.Printf("\nNro. Comercio: ")
	fmt.Scanf("%v", &comercio)
	fmt.Printf("\nMonto: ")
	fmt.Scanf("%v", &monto)
	fmt.Printf("\n")

	rows, err := db.Query(`SELECT realizarCompra($1,$2,$3,$4)`, tarjeta, codseguridad, comercio, monto)
	if err != nil {
		fmt.Printf("Error. %v\n", err)
	}
	defer rows.Close()
	for rows.Next() {
		/*Guardo el resultado de la compra en 'resultado'. true = good, false = bad*/
		if err := rows.Scan(&resultado); err != nil {
			log.Fatal(err)
		}
		/*Si la compra se hizo bien*/
		if resultado {
			/*Traigo el numero de la operacion*/
			row, err := db.Query(`SELECT nrooperacion FROM compra WHERE fecha = (SELECT MAX(fecha) FROM compra)`)
			if err != nil {
				log.Fatal(err)
			}
			defer row.Close()
			/*Guardo el numero de operacion*/
			for row.Next() {
				if err = row.Scan(&nrooperacion); err != nil {
					log.Fatal(err)
				}
			}
			if err = row.Err(); err != nil {
				log.Fatal(err)
			}
			fmt.Printf("Operacion %v realizada exitosamente\n", nrooperacion)
		} else {
			fmt.Printf("No se pudo realizar la compra\n")
		}
	}
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
}

func establecerCierres(db *sql.DB) {
	var resultado bool

	rows, err := db.Query(`SELECT establecerCierres()`)
	if err != nil {
		fmt.Printf("Error. %v", err)
	}
	defer rows.Close()

	for rows.Next() {
		/*Guardo el resultado de la compra en 'resultado'. true = good, false = bad*/
		if err := rows.Scan(&resultado); err != nil {
			log.Fatal(err)
		} else {
			if resultado {
				fmt.Printf("Cierres establecidos\n")
			} else {
				fmt.Printf("No se pudieron establecer los cierres\n")
			}
		}

	}
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
}

func execSlice(slice []string, db *sql.DB) {
	for _, query := range slice {
		fmt.Printf("%v\n", query)
		_, err := db.Exec(query)
		if err != nil {
			fmt.Printf("Error!. %v", err)
		}
	}
}

/*-------------------- Funciones para limpiar la consola -------------------------*/
var clear map[string]func() //create a map for storing clear funcs

func init() {
	clear = make(map[string]func()) //Initialize it
	clear["linux"] = func() {
		cmd := exec.Command("clear") //Linux example, its tested
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
	clear["windows"] = func() {
		cmd := exec.Command("cmd", "/c", "cls") //Windows example, its tested
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
}

func CallClear() {
	time.Sleep(2 * time.Second)

	value, ok := clear[runtime.GOOS] //runtime.GOOS -> linux, windows, darwin etc.
	if ok {                          //if we defined a clear func for that platform:
		value() //we execute it
	} else { //unsupported platform
		panic("Your platform is unsupported! I can't clear terminal screen :(")
	}
}

/*-----------------Fin funciones para limpiar la consola -------------------------*/
