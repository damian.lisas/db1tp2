CREATE OR REPLACE FUNCTION dosRechazosConsecutivosAlerta() RETURNS TRIGGER AS $$
DECLARE
    cantrechazos int;
    ultimaalerta int;
    iniciodeldia timestamp;
    findeldia timestamp;
BEGIN
    iniciodeldia := current_date + interval '0h';
    findeldia := current_date + interval '23h,59m,59s';

    /*Traigo la cantidad de rechazos de este dia por exeder limite para esta tarjeta con motivo = '?supera el límite de tarjeta'*/
    select count(*) into cantrechazos from alerta 
        where 
            codalerta = 32 
        and iniciodeldia < new.fecha
        and findeldia > new.fecha;
    
    raise notice 'Rechazos = %',cantrechazos;
    
    if cantrechazos > 1 and (select estado from tarjeta where nrotarjeta = new.nrotarjeta) <> 'suspendida' then
        /*Suspendo la tarjeta*/
        update tarjeta set estado = 'suspendida' where nrotarjeta = new.nrotarjeta;
        /*Inserto la alerta*/
        /*Busco el id de la ultima alerta*/
        select max(nroalerta) into ultimaalerta from alerta;
        /*Si no hay alertas el codigo es 0*/
        if ultimaalerta ISNULL then
            ultimaalerta := 0;
        end if;
        insert into alerta values(  ultimaalerta +1,
                                    new.nrotarjeta,
                                    new.fecha, 
                                    new.nrorechazo,
                                    32, 
                                    '?tarjeta rechazada multiples veces por superar el limite en un mismo dia');
    end if;

    return new;
END;
$$ LANGUAGE PLPGSQL;

DROP TRIGGER IF EXISTS dosRechazosConsecutivosAlertaTrigger ON rechazo;
CREATE TRIGGER dosRechazosConsecutivosAlertaTrigger
AFTER INSERT ON rechazo
FOR EACH ROW
EXECUTE PROCEDURE dosRechazosConsecutivosAlerta()