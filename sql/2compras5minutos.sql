/*Analizar la posibilidad de integrar esta funcion dentro del trigger de 2compras1minuto*/
/*2 compras en menos de 5 minutos en comercios con cp diferentes*/
CREATE OR REPLACE FUNCTION doscompras5minutoAlerta() RETURNS TRIGGER AS $$
DECLARE
    compras int;
    ultimaalerta int;
    compraanterior record;
    haycompras BOOLEAN;
    menora5minutos BOOLEAN;
    diferentecp BOOLEAN;
BEGIN
    haycompras := true;
    menora5minutos := false;
    diferentecp := false;
    /*Valido que haya al menos 2 compras para comparar*/
    select count(*) into compras from compra where nrotarjeta = new.nrotarjeta;
    if compras < 2 then
        haycompras := false;
    end if;

    if haycompras then
        /*Obtengo la ante ultima compra*/
        select * into compraanterior from compra where nrooperacion = (select max(b.nrooperacion) from compra b where b.nrooperacion < (select max(nrooperacion) from compra));

        /*pasaron menos de 5 minutos entre ambas compras*/
        if extract(epoch from compraanterior.fecha - new.fecha) < 300 THEN
                menora5minutos := true;
        end if;

        /*tienen diferente cp*/
        if (select count(*) from comercio where nrocomercio in (new.nrocomercio, compraanterior.nrocomercio)) > 1 then
            diferentecp := true;
        end if;

        if (menora5minutos and diferentecp) then
            /*Busco el id de la ultima alerta*/
            select max(nroalerta) into ultimaalerta from alerta;
            /*Si no hay alertas el codigo es 0*/
            if ultimaalerta ISNULL then
                ultimaalerta := 0;
            end if;

            insert into alerta VALUES(  ultimaalerta +1,
                                        new.nrotarjeta,
                                        new.fecha, 
                                        null, /*No tiene numero de rechazo ya que la alerta no se genera a partir de un rechazo*/
                                        5, 
                                        '?2 compras en menos de 5 minuto en Negocios con diferente codigo postal');
        end if;
    end if;
    return new;
END;
$$ LANGUAGE PLPGSQL;

DROP TRIGGER IF EXISTS doscompras5minutoAlertaTrigger on compra;
CREATE TRIGGER doscompras5minutoAlertaTrigger
AFTER INSERT ON compra
FOR EACH ROW
EXECUTE PROCEDURE doscompras5minutoAlerta();