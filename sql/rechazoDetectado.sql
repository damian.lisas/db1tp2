CREATE OR REPLACE FUNCTION insertarAlertaOnRechazo() RETURNS TRIGGER AS $$
DECLARE
    ultimaalerta int;
    codalerta int;
BEGIN
    raise notice 'generando alerta por rechazo';
    /*Busco el id de la ultima alerta*/
    select max(nroalerta) into ultimaalerta from alerta;
    /*Si no hay alertas el codigo es 0*/
    if ultimaalerta ISNULL then
        ultimaalerta := 0;
    end if;

    /*Se supero el limite de la tarjeta*/
    if new.motivo = '?supera el límite de tarjeta' then
        codalerta := 32;
    else 
        codalerta := 0;
    end if;
    INSERT INTO alerta VALUES( ultimaalerta + 1,
                                new.nrotarjeta, 
                                new.fecha, 
                                new.nrorechazo, 
                                codalerta, 
                                new.motivo );
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS insertarAlertaOnRechazoTrigger ON rechazo;
CREATE TRIGGER insertarAlertaOnRechazoTrigger
AFTER INSERT ON rechazo
FOR EACH ROW
EXECUTE PROCEDURE insertarAlertaOnRechazo();
