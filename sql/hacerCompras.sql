create or replace function hacerCompras() returns boolean as $$
declare
    c record;
begin
    /*por cada consumo en consumos, realizo una compra*/
    for c in select * from consumo loop
        perform realizarCompra(c.nrotarjeta, c.codseguridad, c.nrocomercio, c.monto);
    end loop;
    return true;
end;
$$ language plpgsql;