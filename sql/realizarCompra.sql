create or replace function realizarCompra(tarjeta char(16),codseguridad char(4),comercio int,monto decimal(7,2)) returns boolean as $$
declare
    resultadooperacion boolean;
begin
    if comercio < 1 or comercio > 20 then
        raise notice 'comercio no existe';
        return false;
    end if;
    select autorizacionCompra(tarjeta,codseguridad,comercio,monto) into resultadooperacion;
    return resultadooperacion;
end;
$$ language plpgsql;