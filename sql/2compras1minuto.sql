CREATE OR REPLACE FUNCTION doscompras1minutoAlerta() RETURNS TRIGGER AS $$
DECLARE
    compras int;
    ultimaalerta int;
    oldcomercio record;
    haycompras BOOLEAN;
    menora1minuto BOOLEAN;
    comerciosdiferentes BOOLEAN;
    diferentecp BOOLEAN;
BEGIN
    haycompras := true;
    menora1minuto := false;
    comerciosdiferentes := false;
    diferentecp := false;

    select count(*) into compras from compra where nrotarjeta = new.nrotarjeta;
    if compras < 2 then
        haycompras := false;
    end if;
    
    if haycompras THEN
        /*obtengo la ante ultima compra*/
        select * into oldcomercio from compra 
            where 
            nrooperacion = (select max(b.nrooperacion) from compra b where b.nrooperacion < (select max(nrooperacion) from compra));
        /*son comercios diferentes*/
        if new.nrocomercio <> oldcomercio.nrocomercio THEN
            comerciosdiferentes = TRUE;
        end if;

        /*paso menos de 1 minuto entre ambas compras*/
        if extract(epoch from oldcomercio.fecha - new.fecha) < 60 THEN
                menora1minuto := true;
        end if;

        /*tienen diferente cp*/
        if (select count(*) from comercio where nrocomercio in (new.nrocomercio, oldcomercio.nrocomercio)) > 1 then
            diferentecp := true;
        end if;

        if (menora1minuto and comerciosdiferentes and diferentecp) THEN
            /*Busco el id de la ultima alerta*/
            select max(nroalerta) into ultimaalerta from alerta;
            /*Si no hay alertas el codigo es 0*/
            if ultimaalerta ISNULL then
                ultimaalerta := 0;
            end if;

            insert into alerta VALUES(  ultimaalerta +1,
                                        new.nrotarjeta,
                                        new.fecha, 
                                        null, /*No tiene numero de rechazo ya que la alerta no se genera a partir de un rechazo*/
                                        1, 
                                        '?2 compras en menos de 1 minuto en Negocios diferentes');
        end if;
    end if;   

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS doscompras1minutoAlertaTrigger on compra;
CREATE TRIGGER doscompras1minutoAlertaTrigger
AFTER INSERT ON compra
FOR EACH ROW
EXECUTE PROCEDURE doscompras1minutoAlerta();
