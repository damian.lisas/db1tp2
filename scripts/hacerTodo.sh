#!/bin/bash

echo 'Crear Tablas y base de datos'
./crearTablas.sh $1

echo 'Alter tables'
./alterTables.sh $1 $2

echo 'Realizar inserts'
./realizarInserts.sh $1 $2
./insertarConsumos.sh $1 $2

echo 'Crear trigger y stored procedures'
./crearTriggersYProcedimientos.sh $1 $2