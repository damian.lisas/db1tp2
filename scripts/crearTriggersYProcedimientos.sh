#!/bin/bash
cd ../sql/
echo 'Creando triggers y funciones'
echo $1$2
psql -U $1 $2 << OMG
\i 2compras1minuto.sql
\i 2compras5minutos.sql
\i 2rechazosConsecutivos.sql
\i autorizarCompra.sql
\i establecerCierres.sql
\i generarResumen.sql
\i hacerCompras.sql
\i realizarCompra.sql
\i rechazoDetectado.sql
OMG

echo 'Triggers y funciones creadas!'