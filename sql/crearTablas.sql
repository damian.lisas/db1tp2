DROP DATABASE IF EXISTS prueba;
CREATE DATABASE prueba;
\c prueba

-- name: create-cliente-table
CREATE TABLE cliente(
    nrocliente INT,
    nombre TEXT,
    apellido TEXT,
    domicilio TEXT,
    telefono CHAR(12)
);
CREATE TABLE tarjeta(
    nrotarjeta CHAR(16),
    nrocliente INT,
    validadesde CHAR(6), --e.g. 201106
    validahasta CHAR(6),
    codseguridad CHAR(4),
    limitecompra DECIMAL(8,2),
    estado CHAR(10) --`vigente', `suspendida', `anulada'
);
CREATE TABLE comercio(
    nrocomercio INT,
    nombre TEXT,
    domicilio TEXT,
    codigopostal CHAR(8),
    telefono CHAR(12)
);
CREATE TABLE compra(
    nrooperacion INT,
    nrotarjeta CHAR(16),
    nrocomercio INT,
    fecha TIMESTAMP,
    monto DECIMAL(7,2),
    pagado boolean
);
CREATE TABLE rechazo(
    nrorechazo INT,
    nrotarjeta CHAR(16),
    nrocomercio INT,
    fecha TIMESTAMP,
    monto DECIMAL(7,2),
    motivo TEXT
);
CREATE TABLE cierre(
    año INT,
    mes INT,
    terminacion INT,
    fechainicio date,
    fechacierre date,
    fechavto date
);
CREATE TABLE cabecera (
    nroresumen INT,
    nombre TEXT,
    apellido TEXT,
    domicilio TEXT,
    nrotarjeta CHAR(16),
    desde date,
    hasta date,
    vence date,
    total DECIMAL(8,2)
);
CREATE TABLE detalle (
    nroresumen INT,
    nrolinea INT,
    fecha DATE,
    nombrecomercio TEXT,
    monto DECIMAL(7,2)
);
CREATE TABLE alerta (
    nroalerta INT,
    nrotarjeta CHAR(16),
    fecha TIMESTAMP,
    nrorechazo INT,
    codalerta INT, --0:rechazo, 1:compra 1min, 5:compra 5min, 32:límite
    descripcion TEXT
);
-- Esta tabla no es parte del modelo de datos, pero se incluye para
-- poder probar las funciones.
CREATE TABLE consumo(
    nrotarjeta CHAR(16),
    codseguridad CHAR(4),
    nrocomercio INT,
    monto DECIMAL(7,2)
);